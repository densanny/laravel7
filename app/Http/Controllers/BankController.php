<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use RealRashid\SweetAlert\Facades\Alert;
use App\Bank;
use Illuminate\Contracts\Validation\ValidationException;
class BankController extends Controller
{
    //
    public function index ()
    {
        $bank = DB::table('banks')->get();
        return view('banking.index', compact('bank'));
    }

    public function create()
    {
        return view('banking.create');
    }

    public function store(Request $request)
    {
      
        // Validator::make($request->all(), [
        //      'bank_name' => 'required | min:3 | max:7',
        //     'contact_email' => 'required | email |  unique:banks',
        //     'logo' => 'required| image| mimes:jpg,png,jpeg|max:2048' 
        // ])->validate();

        $request->validate([
            'bank_name' => 'required | min:3 | max:7',
            'contact_email' => 'required | email |  unique:banks',
            'logo' => 'required| image| mimes:jpg,png,jpeg|max:2048' 
        ]);

      
        
        $data = array();
        $data['bank_name'] = $request->bank_name;
        $data['contact_email'] = $request->contact_email;
        
        $image= $request->file('logo');
        if ($image) {
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name;
            $success = $image->move($upload_path, $image_full_name);
            $data['logo']= $image_url;
            $bank = DB::table('banks')->insert($data);

          
            Alert::success('success', 'banking created succesfully');
            return redirect()->route('banking.index');
                            // ->with('success', 'banking created successfully');
            

        }

        return $request->input();
    }

    public function edit($id)
    {
        $bank = DB::table('banks')->where('id', $id)->first();
        return view('banking.edit', compact('bank'));
    }

    public function update(Request $request, $id)
    {
    
        // $request->validate([
        //     'bank_name' => 'required | min:3 | max:7',
        //     'contact_email' => 'required | email | max:12 | unique:users',
        //     'logo' => 'required| image| mimes: jpg, png | max:2048',
        // ]);

        $oldlogo = $request->old_logo;
        $data = array();
        $data['bank_name'] = $request->bank_name;
        $data['contact_email'] = $request->contact_email;
        
        $image= $request->file('logo');
        if ($image) {
            unlink($oldlogo);
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'public/media/';
            $image_url = $upload_path.$image_full_name;
            $success = $image->move($upload_path, $image_full_name);
            $data['logo']= $image_url;
            $bank = DB::table('banks')->where('id', $id)->update($data);

            return redirect()->route('banking.index')
                            ->with('success', 'banking updated successfully');


        }

        // return $request->input();
    }
    
    public function delete($id){
        $data = DB::table('banks')->where('id', $id)->first();
        $image = $data->logo;
        unlink($image);
        $bank = DB::table('banks')->where('id', $id)->delete();
        return redirect()->route('banking.index')
                         ->With('success', 'Banking deleted succesfully');
    }






}
