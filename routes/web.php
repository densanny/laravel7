<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('banking', 'BankController@index')->name('banking.index');
Route::get('create', 'BankController@create')->name('banking.create');
Route::post('store', 'BankController@store')->name('banking.store');
Route::get('edit/banking/{id}', 'BankController@edit');
Route::post('update/banking/{id}', 'BankController@update');
Route::get('delete/banking/{id}', 'BankController@delete');


// Route::resource('banks', 'BankController'); 