@extends('banking.layout')
@section('content')
<br><br><br>
    
<div class="row">
    <div class="pull-left">
        <h2>Edit bank</h2>
    </div>

    <div class="pull-right">
    <a href="{{  route('banking.index') }}" class="btn btn-primary"> Back</a>
    </div>
</div>

<form action="{{ url('update/banking/'.$bank->id) }}" method="POST" enctype="multipart/form-data">
@csrf
<div class="row">
    <div class="col-xs-4 col-sm-4 col-md-4">
        <div class="form-group">
            <strong>Bank Name</strong>
        <input type="text" name="bank_name" class="form-control" placeholder="bank name" value="{{ $bank->bank_name }}">
        </div>
    </div>

    <div class="col-xs-4 col-sm-4 col-md-4">
        <div class="form-group">
            <strong>Contact Email</strong>
            <input type="text" name="contact_email" class="form-control" placeholder="contact email" value="{{ $bank->contact_email }}">
        </div>
    </div>

    <div class="col-xs-4 col-sm-4 col-md-4">
        <div class="form-group">
            <strong>Logo</strong>
            <input type="file" name="logo"  >
        </div>
    </div>

    
    <div class="col-xs-4 col-sm-4 col-md-4">
        <div class="form-group">
            <strong> Old Logo</strong>
            <img src="{{ URL::to($bank->logo) }}"  height="100px" width="100px"> 
            <input type="hidden" name="old_logo" value="{{ $bank->logo }}">
        </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6">
<button type="submit" class="btn btn-primary">submit</button>
    </div>

</div>

</form>

@endsection