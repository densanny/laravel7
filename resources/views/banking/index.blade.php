@extends('banking.layout')

@section('content')
<br><br><br>
<div class="row" id="success">
    @if ($message = Session::get('success'))
    <div class="alert alert-success" >
    <p>{{ $message }}</p>   
    </div>
@endif


    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Laravel Banking List</h2>
        </div>

        <div class="pull-right">
        <a href="{{ route('banking.create') }}" class="btn btn-success">Create New Banking</a>
        </div>

    </div>



    <table class="table table-bordered">
        <tr>
           <th width="150px">Banking Name</th>
           <th width="150px">Contact Name</th>
           <th width="150px">Logo</th>
           <th width="100px">Action</th>
        </tr>

        <tr>
            @foreach($bank as $banks)
        <td>{{ $banks->bank_name }}</td>
        <td>{{ $banks->contact_email }}</td>
        <td> <img src="{{ URL::to($banks->logo) }}"  height="100px" width="100px"> </td>
            <td>
            <a href="{{ URL::to('edit/banking/'.$banks->id) }}" class="btn btn-success">Edit</a> 
                  <a href="{{ URL::to('delete/banking/'.$banks->id) }}" class="btn btn-danger"
                    onclick="return confirm('are you sure')"   >Delete</a> 
            </td>        
        </tr>
        @endforeach

    </table>

</div>
<script>
    Swal.fire({
    position: 'top-end',
    icon: 'success',
    title: 'Your work has been saved',
    showConfirmButton: false,
    timer: 1500
})
</script>
    
@endsection

