@extends('banking.layout')
@section('content')
<br><br><br>
    
<div class="row">
    <div class="pull-left">
        <h2>Add new Banking</h2>
    </div>

    <div class="pull-right">
    <a href="{{  route('banking.index') }}" class="btn btn-primary"> Back</a>
    </div>
</div>

<form action="{{ route('banking.store') }}" method="POST"  enctype="multipart/form-data">
@csrf
<div class="row">
    <div class="col-xs-4 col-sm-4 col-md-4">
        <div class="form-group">
            <strong>Bank Name</strong>
            <input type="text" name="bank_name" class="form-control" placeholder="bank name">
            <span class="text-danger">{{ $errors->first('bank_name') }}</span>
        </div>
    </div>

    <div class="col-xs-4 col-sm-4 col-md-4">
        <div class="form-group">
            <strong>Contact Email</strong>
            <input type="text" name="contact_email" class="form-control" placeholder="contact email">
            <span class="text-danger">{{ $errors->first('contact_email') }}</span>
        </div>
    </div>

    <div class="col-xs-4 col-sm-4 col-md-4">
        <div class="form-group">
            <strong>Logo</strong>
            <input type="file" name="logo"  >
            <span class="text-danger">{{ $errors->first('logo') }}</span>
        </div>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-6">
<button type="submit" class="btn btn-primary" >submit</button>
    </div>

</div>

</form>
@endsection